import csv

condenados = csv.reader(open('condenadostcu.csv', 'rb'), delimiter=';')
prefeitos = csv.reader(open('pesquisa_brasil.csv', 'rb'), delimiter=';')

lista_condenados = []
for condenado in condenados:
    lista_condenados.append(condenado[3].split('<br/>')[0].strip().upper())

for prefeito in prefeitos:
    if prefeito[3].upper() in lista_condenados:
        print prefeito[0] + ' - ' + prefeito[1] + ' - ' + prefeito[3]
